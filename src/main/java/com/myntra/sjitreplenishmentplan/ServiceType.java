package com.myntra.sjitreplenishmentplan;

public enum ServiceType {
    MARVIN_BACKEND("marvin"),
    PO_BACKEND("purchase-order");
    private String serviceName;
    private ServiceType(String serviceName){
        this.serviceName = serviceName;
    }
    @Override
    public String toString(){
        return serviceName;
    }
}
