package com.myntra.sjitreplenishmentplan.utils;

import com.myntra.purchaseorder.entry.MarketPlaceOrderEntry;
import com.myntra.purchaseorder.entry.MarketPlaceOrderSKUEntry;

import java.util.ArrayList;
import java.util.List;

public class POPayLoad {



public  List<MarketPlaceOrderEntry> createPO(long vendorID) {

    List<MarketPlaceOrderEntry> marketPlaceOrderEntries = new ArrayList<>();
    MarketPlaceOrderEntry marketPlaceOrderEntry = new MarketPlaceOrderEntry();


    marketPlaceOrderEntry.setVendorId(vendorID);
    marketPlaceOrderEntry.setVendorWarehouseId(96l);
    marketPlaceOrderEntry.setVendorAddressId(1092l);
    marketPlaceOrderEntry.setBuyerId(3974l);
    marketPlaceOrderEntry.setBuyerWarehouseId(36l);
    marketPlaceOrderEntry.setCommercialType("SOR");
    marketPlaceOrderEntry.setSourceReferenceId("768895");

    List<MarketPlaceOrderSKUEntry> marketPlaceOrderSKUEntries = new ArrayList<>();


    MarketPlaceOrderSKUEntry marketPlaceOrderSKUEntry = new MarketPlaceOrderSKUEntry();

    marketPlaceOrderSKUEntry.setSkuId(12070040l);
    marketPlaceOrderSKUEntry.setMrp(100d);
    marketPlaceOrderSKUEntry.setQuantity(10);
    marketPlaceOrderSKUEntries.add(marketPlaceOrderSKUEntry);
    marketPlaceOrderEntry.setMarketPlaceOrderSKUEntries(marketPlaceOrderSKUEntries);
    marketPlaceOrderEntries.add(marketPlaceOrderEntry);

    return marketPlaceOrderEntries;

    }

    public  List<MarketPlaceOrderEntry> createPO(long vendorID, long VendorWarehouseId, long VendorAddressId,
                                                 long BuyerId, long BuyerWarehouseId) {

        List<MarketPlaceOrderEntry> marketPlaceOrderEntries = new ArrayList<>();
        MarketPlaceOrderEntry marketPlaceOrderEntry = new MarketPlaceOrderEntry();


        marketPlaceOrderEntry.setVendorId(vendorID);
        marketPlaceOrderEntry.setVendorWarehouseId(VendorWarehouseId);
        marketPlaceOrderEntry.setVendorAddressId(VendorAddressId);
        marketPlaceOrderEntry.setBuyerId(BuyerId);
        marketPlaceOrderEntry.setBuyerWarehouseId(BuyerWarehouseId);
        marketPlaceOrderEntry.setCommercialType("SOR");
        marketPlaceOrderEntry.setSourceReferenceId("768895");

        List<MarketPlaceOrderSKUEntry> marketPlaceOrderSKUEntries = new ArrayList<>();


        MarketPlaceOrderSKUEntry marketPlaceOrderSKUEntry = new MarketPlaceOrderSKUEntry();

        marketPlaceOrderSKUEntry.setSkuId(12070040l);
        marketPlaceOrderSKUEntry.setMrp(100d);
        marketPlaceOrderSKUEntry.setQuantity(10);
        marketPlaceOrderSKUEntries.add(marketPlaceOrderSKUEntry);
        marketPlaceOrderEntry.setMarketPlaceOrderSKUEntries(marketPlaceOrderSKUEntries);
        marketPlaceOrderEntries.add(marketPlaceOrderEntry);

        return marketPlaceOrderEntries;

    }

}
