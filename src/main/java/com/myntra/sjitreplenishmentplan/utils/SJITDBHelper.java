package com.myntra.sjitreplenishmentplan.utils;

import com.myntra.utils.db.DBUtilities;

import java.util.ArrayList;
import java.util.List;

public class SJITDBHelper {

    private static final String DB_NAME = "myntra_marvin";
    private static final String PO_DB = "purchase_order";


    public Long numberofplans() throws Exception {

       return DBUtilities.getDB(DB_NAME).getJdbcTemplate().queryForObject("select count(*) from `replenishmentPlan` where Date(`created_on`) = CURDATE()", Long.class);

    }

    public List noDuplicatePlans(){
        List list = new ArrayList();
        int numplans = DBUtilities.getDB(DB_NAME).getJdbcTemplate().queryForObject("select count(*) from `replenishmentPlan` where Date(`created_on`) = CURDATE()", Integer.class);
        int id = DBUtilities.getDB(DB_NAME).getJdbcTemplate().queryForObject("select id from replenishmentPlan where Date(`created_on`) = CURDATE() order by id LIMIT 1", Integer.class);
        for(int i=1; i<= numplans; i++){
            list.add(DBUtilities.getDB(DB_NAME).getJdbcTemplate().queryForObject("select `vendor_id` from `replenishmentPlan` where id="+id,Integer.class));
            id++;
        }
        return list;
    }

    public String poStatus(){
        return DBUtilities.getDB(PO_DB).getJdbcTemplate().queryForObject("select status from `purchase_order` where `vendor_id` = 427 and DATE(`created_on`) = CURDATE() limit 1\n", String.class );
    }

    public String poRemarks(){
        return DBUtilities.getDB(PO_DB).getJdbcTemplate().queryForObject("select `remarks` from `purchase_order` where `vendor_id` = 427 and DATE(`created_on`) = CURDATE() limit 1\n", String.class );
    }

    public String poApproved(){
        return DBUtilities.getDB(PO_DB).getJdbcTemplate().queryForObject("select status from `purchase_order` where `vendor_id` = 1200 and DATE(`created_on`) = CURDATE() order by id desc limit 1\n", String.class);
    }

    public String poRemarksforNowarehouse(){
        return DBUtilities.getDB(PO_DB).getJdbcTemplate().queryForObject("select `remarks` from `purchase_order` where `vendor_id` = 1200 and DATE(`created_on`) = CURDATE() order by id desc limit 1\n", String.class );
    }

    public String poSourceName(){
        return DBUtilities.getDB(PO_DB).getJdbcTemplate().queryForObject("select `source_name` from `purchase_order` where `vendor_id` = 1200 and DATE(`created_on`) = CURDATE() order by id desc limit 1\n", String.class );
    }


}
