package com.myntra.sjitreplenishmentplan.sjitui.validators;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class SjitAutomationPageValidator {
    public void poIndentPage(WebElement element, String pageName){

        Assert.assertEquals(element.getText().trim(), pageName.trim());
    }
}
