package com.myntra.sjitreplenishmentplan.sjitui.validators;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

/**
 * Created by 300000929 on 09/02/18.
 */
public class HomePageValidator {

    public void validateLoggedInUserName(WebElement userName, String userNameString) {
        Assert.assertNotNull(userName);
        Assert.assertEquals(userName.getText().trim(),userNameString.trim());
    }

    public void validateSjitPage(WebElement sjitPageName, String pageName) {
        Assert.assertEquals(sjitPageName.getText().trim(), pageName.trim());
    }
}
