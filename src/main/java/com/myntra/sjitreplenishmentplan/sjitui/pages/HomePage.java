package com.myntra.sjitreplenishmentplan.sjitui.pages;

import com.myntra.sjitreplenishmentplan.sjitui.constants.HomePageConstants;
import com.myntra.ui.AbstractBasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/**
 * Created by 300000929 on 09/02/18.
 */
public class HomePage extends AbstractBasePage<HomePage> {

    public HomePage(ThreadLocal<WebDriver> webDriver) {
        super(webDriver);
        locators = loadLocators("home_page.properties");

    }

    public WebElement getLoginUserName() {
        WebElement userName = utils.findElement(getLocator("home.username"));
        return userName;
    }

    @Override
    protected void load() {
        utils.refreshPage();
    }

    @Override
    protected void isLoaded() throws Error {
        String homeHeaderText = utils.getCurrentPageTitle();
        Assert.assertNotNull(homeHeaderText);
        Assert.assertEquals(homeHeaderText.trim().toLowerCase(),
                HomePageConstants.title.toLowerCase());
    }
}
