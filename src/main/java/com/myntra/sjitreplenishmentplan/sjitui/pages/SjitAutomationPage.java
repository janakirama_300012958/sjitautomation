package com.myntra.sjitreplenishmentplan.sjitui.pages;

import com.myntra.sjitreplenishmentplan.sjitui.constants.SjitAutomationConstantPage;
import com.myntra.ui.AbstractBasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.awt.*;

public class SjitAutomationPage extends AbstractBasePage<SjitAutomationPage> {

    public SjitAutomationPage(ThreadLocal<WebDriver> webDriver) {
        super(webDriver);
        locators = loadLocators("sjit_page.properties");

    }

    public void clickSjit(){
        utils.click(getLocator("sjitpage.sjitlink"));
    }

    public WebElement getSjitPageName() {
        WebElement sjitPageName = utils.findElement(getLocator("sjitpage.name"));
        return sjitPageName;
    }

    public void uploadFile(String uploadFile) throws InterruptedException {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(utils.findElement(getLocator("sjitpage.filepath")));
        actions.click();
        actions.sendKeys(uploadFile);
        actions.build().perform();
    }

    public void clickUploadButton(){
        utils.click(getLocator("sjitpage.uploadbutton"));
    }

    public void clickCreatePOButton(){
        utils.click(getLocator("sjitpage.createpobutton"));
        utils.waitForElementToBeVisible(getLocator("sjitpage.pagename"));
    }

    public WebElement getPOIndexpageName(){
       WebElement PoIndexPageName = utils.findElement(getLocator("sjitpage.pagename"));
        return PoIndexPageName;
    }


    @Override
    protected void load() {
    }

    @Override
    protected void isLoaded() throws Error {
        Assert.assertTrue(getCurrentUrl().contains("/unityuser/login"), "page url");

    }
}
