package com.myntra.sjitreplenishmentplan.sjitui.pages;

import com.myntra.ui.AbstractBasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/**
 * This Class contains methods to Login on Jeeves UI
 * 
 */
public class LoginPage extends AbstractBasePage<LoginPage> {

	public LoginPage(ThreadLocal<WebDriver> webDriver) {
		super(webDriver);
		locators = loadLocators("sjitui.login_page.properties");
	}

	public void login(final String username, final String password) {
		utils.sendKeys(getLocator("login.username"), username.trim());
		utils.sendKeys(getLocator("login.password"), password.trim());
		utils.click(getLocator("login.submit"));
		utils.waitForPageLoad();
	}

	public WebElement getloggedinuser(){
		WebElement element = utils.findElement(getLocator("login.loggedinusername"));
		return element;
	}



	@Override
	protected void load() {
	}

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(getCurrentUrl().contains("/unityuser/login"), "page url");

	}

}
