package com.myntra.sjitreplenishmentplan.services;

import com.myntra.api.HTTPHeaders;
import com.myntra.api.HTTPRequestSpecification;
import com.myntra.api.ObjectMapper;
import com.myntra.marvin.client.response.ReplenishmentDataResponse;
import com.myntra.marvin.client.response.VendorConfigurationResponse;
import com.myntra.purchaseorder.entry.MarketPlaceOrderEntry;
import com.myntra.purchaseorder.response.PurchaseOrderResponse;
import com.myntra.sjitreplenishmentplan.ServiceType;
import com.myntra.utils.test_utils.ServicesHelper;


import java.io.File;

import static com.myntra.sjitreplenishmentplan.endpoints.SJITEndPoints.*;

public class SjitService extends ServicesHelper {

    public ReplenishmentDataResponse replenishmentDataResponse() throws Exception {
        HTTPRequestSpecification requestSpecification = new HTTPRequestSpecification(getBasicHeader());
        ReplenishmentDataResponse replenishmentDataResponse = getRestClient().createRequest(ServiceType.MARVIN_BACKEND.toString(), requestSpecification, REPLENISHMENT_PLAN)
                .post().getDeserializeResponse(ReplenishmentDataResponse.class, true);
        return replenishmentDataResponse;

    }


    public  VendorConfigurationResponse vendorConfigurationResponse(String filepath) throws Exception {

        HTTPRequestSpecification requestSpecification = new HTTPRequestSpecification(multiPartHeader());
        requestSpecification.setFile( new File(filepath));
        VendorConfigurationResponse vendorConfigurationResponse = getRestClient().createRequest(ServiceType.MARVIN_BACKEND.toString(),
                requestSpecification,SJIT_VENDOR_CONFIG).uploadFiles()
                .getDeserializeResponse(VendorConfigurationResponse.class, true);
        return vendorConfigurationResponse;
    }

    public PurchaseOrderResponse purchaseOrderResponse(MarketPlaceOrderEntry requestbody) throws Exception {

        HTTPRequestSpecification requestSpecification = new HTTPRequestSpecification(getBasicHeader(), requestbody);
        requestSpecification.setObjectMapper(ObjectMapper.GSON);
        PurchaseOrderResponse purchaseOrderResponse = getRestClient().createRequest(ServiceType.PO_BACKEND.toString(),requestSpecification,PO_CREATION)
                .post().getDeserializeResponse(PurchaseOrderResponse.class, true);
        return purchaseOrderResponse;
    }


    private HTTPHeaders getBasicHeader()
    {
        HTTPHeaders headers = new HTTPHeaders();
        headers.addHeader("Authorization", "Basic YTpI" );
        headers.addHeader("Accept", "application/json");
        headers.addHeader("Content-Type", "application/json");
        return headers;
    }

    private HTTPHeaders multiPartHeader()
    {
        HTTPHeaders headers = new HTTPHeaders();
        headers.addHeader("Authorization", "Basic YTpi" );
        headers.addHeader("Accept", "application/json");
        headers.addHeader("Content-Type", "multipart/form-data");
        return headers;
    }
}
