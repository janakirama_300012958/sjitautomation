package com.myntra.sjitreplenishmentplan.endpoints;

import java.util.ArrayList;

public interface SJITEndPoints  {

  public String  REPLENISHMENT_PLAN = "myntra-marvin-service/marvin/replenishment/replenishmentPlanCreation";
  public String SJIT_VENDOR_CONFIG = "myntra-marvin-service/marvin/vendorconfiguration/importexcel/sjit";
  public String PO_CREATION = "purchase-order-service/po/sjit";


  public static class DATA_FILES {
  //  public static FormDataContentDisposition fileDetail;
    public static final String userdir = System.getProperty("user.dir");

   // /Users/300012958/sjitAutomation/src/main/resources
    public static final String upload_sjitconfig = userdir + "/src/main/resources/SJIT/sjit_vendor_list.xlsx";
    public static final String upload_sjittextfile = userdir + "/src/main/resources/SJIT/sjit_textfile.txt";
    public static final String upload_sjitnodatafile = userdir + "/src/main/resources/SJIT/sjit_vendor_list_No_Data.xlsx";
    public static final String upload_sjitwronglyformattedfile = userdir + "/src/main/resources/SJIT/sjit_vendor_list_wrongly_formatted.xlsx";

    public static ArrayList<String> uploadFiles = new ArrayList<>();

    static{
      uploadFiles.add(upload_sjitconfig);
      uploadFiles.add(upload_sjittextfile);
      uploadFiles.add(upload_sjitnodatafile);
      uploadFiles.add(upload_sjitwronglyformattedfile);
    }

  }

  //public String SJIT_VENDOR_LIST = ""

}
