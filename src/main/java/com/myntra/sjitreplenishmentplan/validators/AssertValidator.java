package com.myntra.sjitreplenishmentplan.validators;


import com.myntra.marvin.client.codes.ReplenishmentSuccessCodes;
import com.myntra.marvin.client.response.ReplenishmentDataResponse;
import com.myntra.marvin.client.response.VendorConfigurationResponse;
import com.myntra.purchaseorder.code.PurchaseOrderSuccessCode;
import com.myntra.purchaseorder.response.PurchaseOrderResponse;
import com.myntra.sjitreplenishmentplan.services.SjitService;
import com.myntra.sjitreplenishmentplan.utils.SJITDBHelper;
import com.myntra.utils.db.DBUtilities;
import com.myntra.utils.logger.ILogger;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssertValidator {


    public static void replenishmentPlanAssertion(ReplenishmentDataResponse replenishmentDataResponse) throws Exception {
        Assert.assertEquals(replenishmentDataResponse.getStatus().getStatusCode(), ReplenishmentSuccessCodes.
                REPLENISHMENT_ENRICHED_CONFIGURATION_ADDED_SUCCESSFULLY.getStatusCode());
        Assert.assertEquals(replenishmentDataResponse.getStatus().getStatusMessage(), ReplenishmentSuccessCodes.
                REPLENISHMENT_ENRICHED_CONFIGURATION_ADDED_SUCCESSFULLY.getStatusMessage());
        Assert.assertEquals(replenishmentDataResponse.getStatus().getTotalCount(), new SJITDBHelper().numberofplans().longValue());

    }

    public static void planExist(ReplenishmentDataResponse replenishmentDataResponse) {
        Assert.assertEquals(replenishmentDataResponse.getStatus().getStatusCode(), ReplenishmentSuccessCodes.
                REPLENISHMENT_ENRICHED_CONFIGURATION_ALREADY_ADDED.getStatusCode());
        Assert.assertEquals(replenishmentDataResponse.getStatus().getStatusMessage(), ReplenishmentSuccessCodes.
                REPLENISHMENT_ENRICHED_CONFIGURATION_ALREADY_ADDED.getStatusMessage());

    }

    /* Sucess messages are not present in ReplenishmentSuccessCodes */

    public static void vendorConfigAssertion(VendorConfigurationResponse vendorConfigurationResponse) {

        Assert.assertEquals(vendorConfigurationResponse.getStatus().getStatusCode(), 19000);
        Assert.assertEquals(vendorConfigurationResponse.getStatus().getStatusMessage(), "Vendor Configuration Received Successfully");

    }

    public static void createPOAssertion(PurchaseOrderResponse purchaseOrderResponse) throws Exception {

        Assert.assertEquals(purchaseOrderResponse.getStatus().getStatusCode(), PurchaseOrderSuccessCode.PO_CREATED_FOR_SJIT_ORDER_ENTRY.getStatusCode());
        Assert.assertEquals(purchaseOrderResponse.getStatus().getStatusMessage(), PurchaseOrderSuccessCode.PO_CREATED_FOR_SJIT_ORDER_ENTRY.getStatusMessage());
    }

    public static void noDupPlanValidator()  {
        int k =0;
        Object i=0;
        int j=0;
        SJITDBHelper plans = new SJITDBHelper();
        List li = plans.noDuplicatePlans();
        Map<Object, Integer> map = new HashMap<>();

        for (Object temp : li) {
            Integer count = map.get(temp);
            map.put(temp, (count == null) ? 1 : count + 1);
        }

        for (Map.Entry<Object, Integer> entry : map.entrySet()) {
            j=0;


            if(entry.getValue()>1){

                k = entry.getValue();
                i = entry.getKey();
                Assert.fail("Test case failed because, duplicate Plan created for vendor " +i+". number of Duplicate Plan is "+k);
            }

        }

    }

    public static void poAborted(){
        Assert.assertEquals(new SJITDBHelper().poStatus(), "ABORTED");
        Assert.assertEquals(new SJITDBHelper().poRemarks().toLowerCase(), "purchase order validation/enrichment failed becasue: vendor does not exist or is not approved");
    }

    public static void poApproved(PurchaseOrderResponse purchaseOrderResponse) throws Exception {
        Thread.sleep(120000);
        Assert.assertEquals(purchaseOrderResponse.getStatus().getStatusCode(), PurchaseOrderSuccessCode.PO_CREATED_FOR_SJIT_ORDER_ENTRY.getStatusCode());
        Assert.assertEquals(purchaseOrderResponse.getStatus().getStatusMessage(), PurchaseOrderSuccessCode.PO_CREATED_FOR_SJIT_ORDER_ENTRY.getStatusMessage());
        Assert.assertEquals(new SJITDBHelper().poApproved(), "APPROVED");
        Assert.assertEquals(new SJITDBHelper().poSourceName(), "SJIT_PURCHASE_ORDER");

    }

    public static void noPartnerWarehouse(PurchaseOrderResponse purchaseOrderResponse){
        Assert.assertEquals(purchaseOrderResponse.getStatus().getStatusCode(), PurchaseOrderSuccessCode.PO_CREATED_FOR_SJIT_ORDER_ENTRY.getStatusCode());
        Assert.assertEquals(purchaseOrderResponse.getStatus().getStatusMessage(), PurchaseOrderSuccessCode.PO_CREATED_FOR_SJIT_ORDER_ENTRY.getStatusMessage());
        Assert.assertEquals(new SJITDBHelper().poApproved(), "ABORTED");
        Assert.assertEquals(new SJITDBHelper().poRemarksforNowarehouse().toLowerCase(), "purchase order validation/enrichment failed becasue: partner warehouse does not exist".toLowerCase());
    }

}

