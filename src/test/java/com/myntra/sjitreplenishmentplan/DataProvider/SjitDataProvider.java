package com.myntra.sjitreplenishmentplan.DataProvider;

import com.myntra.purchaseorder.entry.MarketPlaceOrderEntry;
import com.myntra.purchaseorder.entry.PurchaseOrderEntry;
import com.myntra.sjitreplenishmentplan.endpoints.SJITEndPoints;
import com.myntra.sjitreplenishmentplan.utils.POPayLoad;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SjitDataProvider {

    @DataProvider(name = "FilePath")
    public static Object[][] multipartBuilder() throws Exception {


        Object[][] data = new Object[SJITEndPoints.DATA_FILES.uploadFiles.size()][1];

        int i = 0;

        for (String name: SJITEndPoints.DATA_FILES.uploadFiles){
            data[i++][0] = name;
        }

        return data;
    }

    @DataProvider(name="createPO")
    public static Iterator<Object[]>  createPo() throws Exception {
        POPayLoad poPayLoad = new POPayLoad();
        List<MarketPlaceOrderEntry> request = poPayLoad.createPO(427l);
        List<Object[]> list = new ArrayList<>();
        for(MarketPlaceOrderEntry li:request){
            list.add(new Object[]{li});
        }
        return list.iterator();

    }

    @DataProvider(name="poApproved")
    public static Iterator<Object[]> poApproved(){
        POPayLoad poPayLoad = new POPayLoad();
        List<MarketPlaceOrderEntry> request = poPayLoad.createPO(1200l);
        List<Object[]> list = new ArrayList<>();
        for(MarketPlaceOrderEntry li:request){
            list.add(new Object[]{li});
        }
        return list.iterator();
    }

    @DataProvider(name="noPartnerWarehouse")
    public static Iterator<Object[]> noPartnerWarehouse(){
        POPayLoad payLoad = new POPayLoad();
        List<MarketPlaceOrderEntry> list = payLoad.createPO(1200l, 96l,
                1092l, 432l, 36);
        List<Object[]> market = new ArrayList<>();

        for(MarketPlaceOrderEntry m : list){
            market.add(new Object[]{m});
        }
        return market.iterator();
    }

}
