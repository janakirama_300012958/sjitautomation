package com.myntra.sjitreplenishmentplan.Tests;

import com.myntra.marvin.client.response.ReplenishmentDataResponse;
import com.myntra.marvin.client.response.VendorConfigurationResponse;
import com.myntra.purchaseorder.entry.MarketPlaceOrderEntry;
import com.myntra.purchaseorder.response.PurchaseOrderResponse;
import com.myntra.sjitreplenishmentplan.DataProvider.SjitDataProvider;
import com.myntra.sjitreplenishmentplan.SJITInitialiser;
import com.myntra.sjitreplenishmentplan.ServiceType;
import com.myntra.sjitreplenishmentplan.utils.SJITDBHelper;
import com.myntra.sjitreplenishmentplan.validators.AssertValidator;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SjitReplenishmentPlan extends SJITInitialiser {

    /* Upload Vendor Configuration, Upload wrong type file, Run API without any data */

    @Test(dataProvider = "FilePath", dataProviderClass = SjitDataProvider.class)
    public void vendorConfig(String filepath) throws Exception {
        SJITDBHelper plans = new SJITDBHelper();
        plans.numberofplans();
        VendorConfigurationResponse vendorConfigurationResponse =  fpServices.vendorConfigurationResponse(filepath);
        AssertValidator.vendorConfigAssertion(vendorConfigurationResponse);
    }

    /* ReplenishmentPlan API
     * PO should not be created for 2nd API run */

    @Test
    public void replenishmentPlan() throws Exception {
        SJITDBHelper plans = new SJITDBHelper();
        ReplenishmentDataResponse replenishmentDataResponse;
        if (plans.numberofplans() == 0) {
            replenishmentDataResponse = fpServices.replenishmentDataResponse();
            AssertValidator.replenishmentPlanAssertion(replenishmentDataResponse);

        }
        else if(plans.numberofplans() > 0){
            replenishmentDataResponse = fpServices.replenishmentDataResponse();
            AssertValidator.planExist(replenishmentDataResponse);
        }
    }

    /* More than one Plan should not be created for one vendor */

    @Test
    public void noDuplicatePlan() {
        AssertValidator.noDupPlanValidator();
    }

    /* PO Created successfully */

    @Test(dataProvider = "createPO", dataProviderClass = SjitDataProvider.class)
    public void poCreation(MarketPlaceOrderEntry marketPlaceOrderEntry) throws Exception{
        PurchaseOrderResponse purchaseOrderResponse = fpServices.purchaseOrderResponse(marketPlaceOrderEntry);
        AssertValidator.createPOAssertion(purchaseOrderResponse);
    }

    /* Testing PO for Aborted status and it's remarks */
    @Test
    public void poAborted(){
        AssertValidator.poAborted();
    }

    /* Testing for PO in Approved status */
    @Test(dataProvider = "poApproved", dataProviderClass = SjitDataProvider.class)
    public void poApproved(MarketPlaceOrderEntry marketPlaceOrderEntry) throws Exception {
         PurchaseOrderResponse purchaseOrderResponse = fpServices.purchaseOrderResponse(marketPlaceOrderEntry);
        AssertValidator.poApproved(purchaseOrderResponse);
    }


    /* Testing PO for Partner warehouse does not exist condition */
    @Test(dataProvider = "noPartnerWarehouse", dataProviderClass = SjitDataProvider.class)
    public void noPartnerWarehouse(MarketPlaceOrderEntry marketPlaceOrderEntry) throws Exception {
        PurchaseOrderResponse poResponse = fpServices.purchaseOrderResponse(marketPlaceOrderEntry);
        AssertValidator.noPartnerWarehouse(poResponse);
    }


}
