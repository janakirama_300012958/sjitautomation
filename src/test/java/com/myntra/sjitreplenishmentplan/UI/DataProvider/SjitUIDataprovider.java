package com.myntra.sjitreplenishmentplan.UI.DataProvider;

import org.testng.annotations.DataProvider;

public class SjitUIDataprovider {

    @DataProvider(name = "login")
    public static Object[][] login() {

        Object[][] data = new Object[1][2];
        data[0][0] = "PO_FINANCE";
        data[0][1] = "benetton";

        return data;
    }

    @DataProvider(name = "sjitpage")
    public static Object[][] sjitpage() {

        Object[][] data = new Object[1][3];
        data[0][0] = "PO_FINANCE";
        data[0][1] = "benetton";
        data[0][2] = "SJIT Automation";

        return data;
    }

    @DataProvider(name = "uploadVendor")
    public static Object[][] uploadVendor(){
        Object[][] data = new Object[1][3];
        data[0][0] = "PO_FINANCE";
        data[0][1] = "benetton";
        data[0][2] = "SJIT Automation";

        return data;

    }

    @DataProvider(name = "POIndent")
    public static Object[][] POIndent() {

        Object[][] data = new Object[1][3];
        data[0][0] = "PO_FINANCE";
        data[0][1] = "benetton";
        data[0][2] = "List of Purchase Order Indents";

        return data;
    }
}
