package com.myntra.sjitreplenishmentplan.UI;

import com.myntra.sjitreplenishmentplan.sjitui.pages.HomePage;
import com.myntra.sjitreplenishmentplan.sjitui.pages.LoginPage;
import com.myntra.sjitreplenishmentplan.sjitui.pages.SjitAutomationPage;
import com.myntra.sjitreplenishmentplan.sjitui.validators.HomePageValidator;
import com.myntra.sjitreplenishmentplan.sjitui.validators.SjitAutomationPageValidator;
import com.myntra.utils.test_utils.BaseTest;
import org.testng.annotations.BeforeMethod;

public class SjitUIInitialiser extends BaseTest {

    public LoginPage loginPage;
    public HomePage homePage;
    public HomePageValidator homePageValidator;
    public SjitAutomationPage sjitAutomationPage;
    public SjitAutomationPageValidator sjitAutomationPageValidator;

    @BeforeMethod
    public void init()
    {
        loginPage = new LoginPage(driver).get();
        homePage = new HomePage(driver);
        homePageValidator = new HomePageValidator();
        sjitAutomationPage = new SjitAutomationPage(driver);
        sjitAutomationPageValidator = new SjitAutomationPageValidator();
    }
}


