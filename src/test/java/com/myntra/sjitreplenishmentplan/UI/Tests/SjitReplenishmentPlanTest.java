package com.myntra.sjitreplenishmentplan.UI.Tests;

import com.myntra.sjitreplenishmentplan.UI.DataProvider.SjitUIDataprovider;
import com.myntra.sjitreplenishmentplan.UI.SjitUIInitialiser;
import com.myntra.sjitreplenishmentplan.sjitui.constants.SjitAutomationConstantPage;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class SjitReplenishmentPlanTest extends SjitUIInitialiser {

    /* User logged into the system */
    @Test(dataProvider = "login", dataProviderClass = SjitUIDataprovider.class)
    public void LoginToJeeves(String username, String password) throws InterruptedException {
        loginPage.login(username, password);
        WebElement element = loginPage.getloggedinuser();
        homePageValidator.validateLoggedInUserName(element, username);
    }

    /* User navigating to SJIT Page */
    @Test(dataProvider = "sjitpage", dataProviderClass = SjitUIDataprovider.class)
    public void clickSjitAutomation(String username, String password, String pageName) throws InterruptedException {
        loginPage.login(username, password);
        WebElement element = loginPage.getloggedinuser();
        homePageValidator.validateLoggedInUserName(element, username);
        sjitAutomationPage.clickSjit();
        WebElement SjitPageName = sjitAutomationPage.getSjitPageName();
        homePageValidator.validateSjitPage(SjitPageName, pageName);
    }

    /* User uploading vendor configuration sheet */
    @Test(dataProvider = "uploadVendor", dataProviderClass = SjitUIDataprovider.class)
    public void uploadVendor(String username, String password, String pageName) throws InterruptedException {
        loginPage.login(username, password);
        WebElement element = loginPage.getloggedinuser();
        homePageValidator.validateLoggedInUserName(element, username);
        sjitAutomationPage.clickSjit();
        WebElement SjitPageName = sjitAutomationPage.getSjitPageName();
        homePageValidator.validateSjitPage(SjitPageName, pageName);
        System.out.println("Sjit file path "+SjitAutomationConstantPage.filePath);
        sjitAutomationPage.uploadFile(SjitAutomationConstantPage.filePath);
        sjitAutomationPage.clickUploadButton();
        Thread.sleep(1000);
    }

    /* User creation POs */
    @Test(dataProvider = "POIndent", dataProviderClass = SjitUIDataprovider.class)
    public void createPO(String username, String password, String pageName) throws InterruptedException {
        loginPage.login(username, password);
        WebElement element = loginPage.getloggedinuser();
        homePageValidator.validateLoggedInUserName(element, username);
        sjitAutomationPage.clickSjit();
        WebElement SjitPageName = sjitAutomationPage.getSjitPageName();
        homePageValidator.validateSjitPage(SjitPageName, pageName);
        sjitAutomationPage.clickCreatePOButton();
        WebElement POIndentName = sjitAutomationPage.getPOIndexpageName();
        sjitAutomationPageValidator.poIndentPage(POIndentName, pageName);
    }
}
