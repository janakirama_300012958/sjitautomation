package com.myntra.sjitreplenishmentplan;

import com.myntra.sjitreplenishmentplan.services.SjitService;
import com.myntra.utils.logger.ILogger;
import com.myntra.utils.test_utils.BaseTest;
import org.testng.annotations.BeforeClass;

public class SJITInitialiser extends BaseTest implements ILogger {

    protected SjitService fpServices;
    @BeforeClass(alwaysRun = true)
    public void init() {
        fpServices = new SjitService();
    }
}
